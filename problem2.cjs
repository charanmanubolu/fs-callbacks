/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require('fs')
const path = require('path')

const array = [
    path.join(__dirname, 'UppercaseFile.txt'),
    path.join(__dirname, 'lowerCaseFile.txt'),
    path.join(__dirname, 'sortFile.txt')
]

function problem2(inputFile, cb) {
    try {
        fs.readFile(inputFile, 'utf-8', (err, data) => {
            if (err) {
                console.log(err)
            }
            let upperCaseData = data.toLocaleUpperCase()

            fs.writeFile('./UppercaseFile.txt', upperCaseData, 'utf8', (err) => {
                if (err) {
                    console.log(err)
                }

                const lowerCaseData = upperCaseData.toLocaleLowerCase();
                const sentences = lowerCaseData.split('.').filter(sentence => sentence.trim() !== '');
                const sentencesData = sentences.join('.\n') + '.';

                fs.writeFile('./lowerCaseFile.txt', sentencesData, 'utf8', (err) => {
                    if (err) {
                        console.log(err)
                    }

                    const sortedSentences = sentencesData.split('.\n').sort().join('.\n') + '.';
                    fs.writeFile('./sortFile.txt', sortedSentences, 'utf8', (err) => {
                        if (err) {
                            console.log(err)
                        }
                        console.log("files created")
                        // cb(array);
                    })
                })
            })
        })

    } catch (error) {
        console.log(error)
    }

}



function deletingTheFile(array) {
    try {
        array.map(path => {
            fs.unlink(path, (err) => {
                if (err) {
                    console.log(err)
                }
                console.log("deleted all files")
            })
        })
    } catch (err) {
        console.log(err)
    }

}

module.exports = { problem2, deletingTheFile }