/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/


const fs = require('fs')
const path = require('path')
const randomFiles1 = path.join(__dirname, './-randomFiles.js')

function createFiles(cb) {
    try {
        for (let index = 0; index < 5; index++) {

            fs.writeFile(`${index}randomFiles1`, 'w', "utf8", (err) => {
                if (err) {
                    throw err;
                }
            })
            console.log('file created')

        }
        cb();

    } catch (err) {
        console.log(err)
    }
}


function deleteFiles() {
    try {
        for (let index = 0; index < 5; index++) {

            fs.unlink(`${index}randomFiles1`, (err) => {
                if (err) {
                    throw err
                }
            })
            console.log('file removed')
        }
    } catch (err) {
        console.log(err)
    }
}

module.exports = { createFiles, deleteFiles };